﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace CriptoFinanceiraWeb.Models
{
    public class CadastroModel
    {
//        [Required(ErrorMessage = "O CPF deve ser informado.!")]
        public string cpf { get; set; }
        public string nome { get; set; }
        public string data_nascimento { get; set; }
        public string nome_mae { get; set; }

        //     [Required(ErrorMessage = "O CEP deve ser informado.!")]
        //     [RegularExpression(@"^\d{8}$|^\d{5}-\d{3}$", ErrorMessage = "O código postal deverá estar no formato 00000000 ou 00000-000")]
        //       [DisplayName("CEP")]
        public string cep { get; set; }
        public string endereco { get; set; }
        public string numero_residencia { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }
        public string senha { get; set; }
        public string complemento { get; set; }
        public string sexo { get; set; }
        public string celular { get; set; }

    }
}