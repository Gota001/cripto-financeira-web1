﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using CriptoFinanceiraWeb.Models;
using CriptoFinanceiraWeb.DAL;

namespace CriptoFinanceiraWeb.Controllers
{

    public class CadastroController : Controller
    {

        CadastroModel cadastro = new CadastroModel();
        // GET: Cadastro
        public ActionResult Cadastro()
        {


            return View();

        }


        public ActionResult Dados()
        {

            return View();
            
        }

         [HttpPost]
         public void Prosseguir()
         {
 //           var cadastro = new CadastroModel();
              cadastro.cpf = Request["cpf"];
        
              if (cadastro.cpf == null || cadastro.cpf.Equals(""))
              {
                  ModelState.AddModelError("cpf", "Favor Preencher o cpf");
                  Response.Redirect(nameof(Cadastro));
               }
              else
              {
                  Response.Redirect(nameof(Dados));
              }

          }
        [HttpPost]
        public ActionResult Cadastrar()
        {
            try
            {
                ClientesDao bd = new ClientesDao();

                cadastro.nome = Request["nome"];
                cadastro.sexo = Request["sexo"].Substring(0,1);
                cadastro.data_nascimento = Request["data_nascimento"];
                cadastro.nome_mae = Request["nome_mae"];
                cadastro.cpf = Request["cpf"];
                cadastro.cep = Request["cep"];
                cadastro.endereco = Request["endereco"];
                cadastro.numero_residencia = Request["numero"];
                cadastro.complemento = Request["complemento"];
                cadastro.bairro = Request["bairro"];
                cadastro.cidade = Request["cidade"];
                cadastro.estado = Request["estado"];
                cadastro.pais = Request["pais"];
                cadastro.telefone = Request["telefone"];
                cadastro.celular = Request["celular"];
                cadastro.email = Request["email"];
                cadastro.senha = Request["senha"];

                String[] campos = {cadastro.nome, cadastro.sexo, cadastro.cpf, cadastro.data_nascimento, cadastro.cep, cadastro.endereco, cadastro.numero_residencia, cadastro.complemento, cadastro.bairro, cadastro.cidade, cadastro.estado, cadastro.pais, cadastro.telefone, cadastro.celular, cadastro.nome_mae, cadastro.email, cadastro.senha };
                bool campoVazio = false;
                
                foreach(String campo in campos)
                {
                    if (campo.Equals("") || campo == String.Empty)
                    {
                        campoVazio = true;
                    }
                }

                if (campoVazio)
                {
                    ViewBag.mensagemCadastro = "Preencha todos os campos";
                }
                else {

                    if (bd.inserirCliente(cadastro.nome, cadastro.sexo, cadastro.cpf, cadastro.data_nascimento, cadastro.cep, cadastro.endereco, cadastro.numero_residencia, cadastro.complemento, cadastro.bairro, cadastro.cidade, cadastro.estado, cadastro.pais, cadastro.telefone, cadastro.celular, cadastro.nome_mae, cadastro.email, cadastro.senha))
                    {
                        ViewBag.mensagemCadastro = "Cadastro Efetuado com sucesso! Efetue login com seus dados!";
                    }
                    else
                    {
                        ViewBag.mensagemCadastro = bd.mensagem;
                    }

                }
                return View("Dados");
            }
            catch
            {
                return View("Dados");
            }

        }

    }
}
