﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CriptoFinanceiraWeb.Models;
using CriptoFinanceiraWeb.DAL;

namespace CriptoFinanceiraWeb.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente
        public ActionResult MenuCliente(LoginModel login)
        {
            

            String aux = "";
            String auxCarteira = "";
            String auxOrdens = "";

 //           if (login.chave_cliente != null){
 //               int index = login.chave_cliente.IndexOf(" ");
 //               int aux1 = index - 2;
 //               aux = login.chave_cliente.Substring(3, aux1);
 //           }
 //           else
            
                aux = Session["Nome"].ToString().Substring(3, Session["Nome"].ToString().IndexOf(" ") - 2);
                auxCarteira = "R$ " + Session["Carteira"].ToString();
                auxOrdens = "R$ " + Session["Ordens"].ToString();

                ViewBag.nomeCliente = "Olá, " + aux + "!";
                ViewBag.saldoCarteira = auxCarteira;
                ViewBag.saldoOrdens = auxOrdens;

            return View();
        }

        public ActionResult Comprar(LoginModel login)
        {

            return View();
        }

        public ActionResult Vender(LoginModel login)
        {
            return View();
        }

        public ActionResult Depositar(LoginModel login)
        {
            return View();
        }

        public ActionResult Transferir(LoginModel login)
        {
            return View();
        }

        [HttpPost]
        public ActionResult ComprarMoeda()
        {
            String moeda = Request["select"].Substring(0, Request["select"].IndexOf(")")) + ")";
            String quantidade = Request["quantidade"];

            ClientesDao cliente = new ClientesDao();
            if(cliente.comprarMoeda(moeda, quantidade, Session["Nome"].ToString()))
            {
                ViewBag.mensagemCompra = cliente.mensagem;
            }
            else
            {
                ViewBag.mensagemErro = cliente.mensagem;
                
            }
            return View("Comprar");
        }

        [HttpPost]
        public ActionResult VenderMoeda()
        {
            String moeda = Request["select"].Substring(0, Request["select"].IndexOf(")")) + ")";
            String quantidade = Request["quantidade"];

            ClientesDao cliente = new ClientesDao();
            if (cliente.comprarMoeda(moeda, quantidade, Session["Nome"].ToString()))
            {
                ViewBag.mensagemCompra = cliente.mensagem;
            }
            else
            {
                ViewBag.mensagemErro = cliente.mensagem;

            }
            return View("Vender");
        }
    }
}