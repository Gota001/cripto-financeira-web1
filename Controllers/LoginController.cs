﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CriptoFinanceiraWeb.DAL;
using CriptoFinanceiraWeb.Models;


namespace CriptoFinanceiraWeb.Controllers
{
    public class LoginController : Controller
    {
 //       LoginModel model = new LoginModel();

        public ActionResult Login(LoginModel model)
        {
 //         ViewBag.erroLogin = model.mensagemErro;
            return View();

        }


        [HttpGet]
        public ActionResult Logar(LoginModel model)
        {
            LoginDao login = new LoginDao();

            model.cpf = Request["cpf"];
            model.senha = Request["senha"];

            if (login.verificarLogin(model.cpf, model.senha))
            {
                //                Response.Redirect(nameof(MenuCliente));
                model.chave_cliente = login.nome_cliente;
                Session["Nome"] = login.nome_cliente;
                Session["Carteira"] = login.valor_carteira;
                Session["Ordens"] = login.valor_em_ordens;

                //                String aux = login.nome_cliente.Substring(2, login.nome_cliente.IndexOf(" "));
                //                ViewBag.nomeCliente = "Olá," + aux + "!";
                return RedirectToAction("MenuCliente", "Cliente", model);
            }
            else
            {
                model.mensagemErro = login.mensagem;
                ViewBag.erroLogin = model.mensagemErro;
                //               Response.Redirect(nameof(Login));
                return View("Login", model);
            }
        }
    }
}