﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CriptoFinanceiraWeb.Models;
using Newtonsoft.Json;

namespace CriptoFinanceiraWeb.Controllers
{   
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Sua Pagina Sobre.";

            return View();
        }

        public ActionResult Cadastro()
        {
            ViewBag.Message = "Sua pagina de cadastro.";


            return View();
        }

        public ActionResult Teste()
        {
            ViewBag.Message = "Your teste page.";

            return View();
        }

        //       [HttpPost]
        //        public void Prosseguir()
        //        {
        //            var cadastro = new CadastroModel();
        //            cadastro.cpf = Request["cpf"];
        //
        //            if (cadastro.cpf == null || cadastro.cpf.Equals(""))
        //            {
        //                ModelState.AddModelError("cpf", "Favor Preencher o cpf");
        //                Response.Redirect(nameof(Cadastro));
        //            }
        //            else
        //            {
        //                Response.Redirect("Dados");
        //            }

        //        }

    }
    
}