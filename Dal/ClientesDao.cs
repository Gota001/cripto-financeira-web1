﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CriptoFinanceiraWeb.DAL;
using System.Data;

namespace CriptoFinanceiraWeb.DAL
{
    class ClientesDao
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        ConexaoBD con = new ConexaoBD();

        SqlDataReader dr;


        public DataTable consultarTodosClientes()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "Select * from tbClientes";
                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar clientes no banco de Dados!";
            }
            return dt;
        }

        public DataTable consultarTodosInvestimentos()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "Select * from tbInvestimentos where status_investimento <> 'aguardando retorno hash'";
                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar clientes no banco de Dados!";
            }
            return dt;
        }
        public DataTable consultarCliente(String valor_carteira, String nomeCliente)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "";
                SqlConnection conn = new SqlConnection();

                if (nomeCliente != String.Empty)
                {

                    query = "Select * from tbClientes where valor_carteira " + valor_carteira +
                    " and nome_cliente = '" + nomeCliente + "'";

                }
                else if (valor_carteira.Equals("todos"))
                {
                    query = "select * from tbClientes";
                }
                else
                {
                    query = "Select * from tbClientes where valor_carteira " + valor_carteira;
                }

                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
                return dt;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar clientes no banco de Dados!";
                return null;
            }
        }

        public DataTable consultarInvestimentos(String moeda, String nome)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "";
                SqlConnection conn = new SqlConnection();

                if (nome != String.Empty)
                {

                    query = "Select * from tbInvestimentos where status_investimento <> 'aguardando retorno hash' and chave_moeda like '%" + moeda +
                    "%' and chave_cliente like '%" + nome + "%'";

                }
                else if (moeda.Equals("todas"))
                {
                    query = "select * from tbInvestimentos where status_investimento <> 'aguardando retorno hash'";
                }
                else
                {
                    query = "Select * from tbInvestimentos where status_investimento <> 'aguardando retorno hash' and chave_moeda like '%" + moeda + "%'";
                }

                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
                return dt;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar investimentos no banco de Dados!";
                return null;
            }
        }

        public bool alterarStatusInvestimentos(String user, String alteracao)
        {
            bool tem = false;
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "UPDATE tbInvestimentos SET status_investimento =" + "'" + alteracao + "' where cod_investimento =" + user;
                SqlCommand cmd = new SqlCommand(query, conn);

                conn = con.conectar();
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao alterar status investimentos!";
            }
            return tem;
        }

        public DataTable consultarTodosRetornoHash()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "Select * from tbInvestimentos where status_investimento = 'aguardando retorno hash'";
                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar investimentos no banco de Dados!";
            }
            return dt;
        }
        public DataTable consultarRetornoHash(String moeda, String nome)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "";
                SqlConnection conn = new SqlConnection();

                if (nome != String.Empty)
                {

                    query = "Select * from tbInvestimentos where status_investimento = 'aguardando retorno hash' and chave_moeda like '%" + moeda +
                    "%' and chave_cliente like '%" + nome + "%'";

                }
                else if (moeda.Equals("todas"))
                {
                    query = "select * from tbInvestimentos where status_investimento = 'aguardando retorno hash'";
                }
                else
                {
                    query = "Select * from tbInvestimentos where status_investimento = 'aguardando retorno hash' and chave_moeda like '%" + moeda + "%'";
                }

                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
                return dt;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar investimentos no banco de Dados!";
                return null;
            }
        }

        public bool descontarValorCarteira(String user, String valor, String id_investimento)
        {
            bool tem = false;

            decimal valor_investimento = Convert.ToDecimal(valor);
            decimal valor_carteira;
            String aux_carteira = "";
            String valor_i;

            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "select valor_carteira from tbClientes where chave_cliente ='" + user + "'";
                SqlCommand cmd = new SqlCommand(query, conn);

                conn = con.conectar();
                cmd.Connection = conn;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read()) {
                    aux_carteira = dr["valor_carteira"].ToString();
                }
                conn.Close();
                valor_carteira = Convert.ToDecimal(aux_carteira);
                valor_carteira -= valor_investimento;
                valor_i = Convert.ToString(valor_carteira);
                valor_i = valor_i.Replace(",", ".");

                conn = con.conectar();
                query = "UPDATE tbClientes SET valor_carteira =" + "" + valor_i + " where chave_cliente ='" + user + "'";
                cmd = new SqlCommand(query, conn);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                conn.Close();

                conn = con.conectar();
                query = "UPDATE tbInvestimentos SET status_investimento ='descontado em carteira' where cod_investimento =" + id_investimento;
                cmd = new SqlCommand(query, conn);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                tem = true;

            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao descontar valor na carteira virtual do cliente!";
                tem = false;
            }
            return tem;
        }
        public bool inserirCliente(String nome, String sexo, String cpf, String dataNasc, String cep, String rua, String numero, String complemento, String bairro, String cidade, String estado, String pais, String telefone, String celular, String nomeMae, String email, String senha)
        {            

            try {
                int aux = 0;
                SqlConnection conn = new SqlConnection();
                String chaveCliente = "";
                String query = "select max(cod_cliente) cod_cliente from tbClientes";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn = con.conectar();
                cmd.Connection = conn;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    chaveCliente = dr["cod_cliente"].ToString();
                }
                conn.Close();

                aux = Convert.ToInt32(chaveCliente) + 1;
                chaveCliente = Convert.ToString(aux) + "-" + nome;

                query = "insert tbClientes values ('" + nome + "','" + sexo + "','" + cpf + "','" + dataNasc + "','" + cep + "','" + rua + "','" + numero + "','" + complemento + "','" + bairro + "','" + cidade + "','" + estado + "','" + pais + "','" + telefone + "','" + celular + "','" + nomeMae + "','" + email + "',0,'" + chaveCliente + "','" + senha + "')";
                cmd = new SqlCommand(query, conn);
                conn = con.conectar();
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch(Exception e)
            {
                this.mensagem = "Erro ao cadastrar cliente!";
                return false;
            }
           
        }
        public bool comprarMoeda(String moeda, String quantidade, String chave_cliente)
        {

            try
            {
                String v_investimento = "";
                decimal qtde;
                String aux_moeda = "";
                SqlConnection conn = new SqlConnection();
                decimal valor_moeda;
                decimal valor_investimento;
                decimal valor_carteira;
                String aux_carteira = "";
                String query = "select valor_carteira from tbClientes where chave_cliente ='"+ chave_cliente+"'";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn = con.conectar();
                cmd.Connection = conn;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    aux_carteira = dr["valor_carteira"].ToString();
                }
                conn.Close();
                valor_carteira = Convert.ToDecimal(aux_carteira);


                query = "select * from tbMoeda where chave_moeda = '" +moeda+ "'";
                cmd = new SqlCommand(query, conn);
                conn = con.conectar();
                cmd.Connection = conn;
                dr = cmd.ExecuteReader();

                if(dr.Read()){
                 aux_moeda = dr["valor_moeda"].ToString();
                }
                
                valor_moeda = Convert.ToDecimal(aux_moeda);
                qtde = Convert.ToDecimal(quantidade);
                valor_investimento = valor_moeda * qtde;
                conn.Close();

                if (valor_carteira > valor_investimento)
                {
                    aux_moeda = aux_moeda.Replace(",",".");
                    quantidade = quantidade.Replace(",", ".");

                    v_investimento = valor_investimento.ToString();
                    v_investimento = v_investimento.Replace(",", ".");

                    query = "insert into tbInvestimentos values ('" + chave_cliente + "','" + moeda + "'," + aux_moeda + "," + quantidade + ",getdate()," + v_investimento + ",'pendente','compra')";
                    cmd = new SqlCommand(query, conn);
                    conn = con.conectar();
                    cmd.Connection = conn;
                    cmd.ExecuteNonQuery();

                    this.mensagem = "Solicitação de compra encaminhada com sucesso aos nossos corretores! Valor Total Compra: " +v_investimento;
                    return true;
                }
                else
                {
                    this.mensagem = "Saldo insuficiente em carteira para realizar essa transação!";
                    return false;

                }

            }
            catch (Exception e)
            {
                this.mensagem = "Erro ao inserir investimento na base!";
                return false;
            }

        }

        public bool venderMoeda(String moeda, String quantidade, String chave_cliente)
        {

            try
            {
                String status_investimento = "";
                String valor_investimento = "";

                SqlConnection conn = new SqlConnection();
                String query = "select chave_moeda from tbInvestimentos where chave_cliente ='" + chave_cliente + "' and chave_moeda = '" + moeda + "' and quantidade =" + quantidade.Replace(",", ".");
                SqlCommand cmd = new SqlCommand(query, conn);
                conn = con.conectar();
                cmd.Connection = conn;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    valor_investimento = dr["valor_investimento"].ToString();
                    status_investimento = dr["status_investimentos"].ToString();
                }
                conn.Close();

                if (!status_investimento.Equals("descontado em carteira"))
                {
                    mensagem = "Você não possui esse tipo de moeda para venda!";
                    return false;
                }
                else
                {
                    String aux_carteira = "";
                    query = "select valor_carteira from tbClientes where chave_cliente ='" + chave_cliente + "'";
                    cmd = new SqlCommand(query, conn);
                    conn = con.conectar();
                    cmd.Connection = conn;
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        aux_carteira = dr["valor_carteira"].ToString();
                    }

                    conn.Close();
                    decimal valor_carteira = Convert.ToDecimal(aux_carteira);
                    decimal v_investimento = Convert.ToDecimal(valor_investimento);
                    valor_carteira += v_investimento;

                    aux_carteira = valor_carteira.ToString();

                    conn = con.conectar();
                    query = "UPDATE tbClientes SET valor_carteira =" + "" + aux_carteira + " where chave_cliente ='" + chave_cliente + "'";
                    cmd = new SqlCommand(query, conn);
                    cmd.Connection = conn;
                    cmd.ExecuteNonQuery();
                    conn.Close();

                    conn = con.conectar();
                    query = "UPDATE tbInvestmentos SET status_investimento = 'vendido' SET tipo_investimento = 'venda' where chave_cliente ='" + chave_cliente + "'";
                    cmd = new SqlCommand(query, conn);
                    cmd.Connection = conn;
                    cmd.ExecuteNonQuery();
                    conn.Close();

                    mensagem = "Venda efetivada com sucesso!";
                    return true;
                }
            }

            catch (Exception e)
            {
                this.mensagem = "Erro ao atualizar investimento na base!";
                return false;
            }

        }

    }
}
