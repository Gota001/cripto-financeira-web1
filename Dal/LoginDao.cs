﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CriptoFinanceiraWeb.DAL
{
    class LoginDao
    {

        public String nome_cliente = "";
        public String valor_carteira = "";
        public String valor_em_ordens = "";

        public bool tem = false;
        public String mensagem ="";
        SqlCommand cmd = new SqlCommand();
        ConexaoBD con = new ConexaoBD();
        SqlDataReader dr;
        public bool verificarLogin(String cpf, String senha)
        {
            cmd.CommandText = "select * from tbClientes where cpf = @cpf and senha = @senha";
            cmd.Parameters.AddWithValue("@cpf",cpf);
            cmd.Parameters.AddWithValue("@senha",senha);

            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    nome_cliente = dr["chave_cliente"].ToString();
                    valor_carteira = dr["valor_carteira"].ToString();
                    valor_em_ordens = dr["valor_em_ordens"]. ToString();
                    tem = true;
                }
                else
                {
                    this.mensagem = "Usuário ou senha inválido!";
                }
            }
            catch (SqlException)
            {
                this.mensagem = "Erro com Banco de Dados!";
            }
            return tem;
        }

        public String cadastrar(String email, String senha, String confSenha)
        {
            return mensagem;
        }
    }
}
